﻿using NUnit.Framework;
using System.Collections.Generic;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {
        private readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void TestTransferBetweenAccounts()
        {
            double amount = 100;
            Account originAccount = new CheckingAccount((int)AccountType.CHECKING);
            Account destinyAccount = new SavingsAccount((int)AccountType.SAVINGS);

            Customer jonathan = new Customer("Jonathan");
            CustomerOperations.OpenAccount(originAccount, jonathan);
            CustomerOperations.OpenAccount(destinyAccount, jonathan);

            originAccount.Deposit(100.0);
            destinyAccount.Deposit(100.0);

            Assert.AreEqual(0, CustomerOperations.TransferToAccount(originAccount, destinyAccount, amount), DOUBLE_DELTA);

        }

        [Test]
        public void TestCustomerStatementGeneration()
        {
            Account checkingAccount = new CheckingAccount((int)AccountType.CHECKING);
            Account savingsAccount = new SavingsAccount((int)AccountType.SAVINGS);

            Customer henry = new Customer("Henry");
            CustomerOperations.OpenAccount(checkingAccount, henry);
            CustomerOperations.OpenAccount(savingsAccount, henry);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            string statement = "Statement for Henry\r\n" +
            "\r\n" +
            "Checking Account\r\n" +
            "  deposit $100,00\r\n" +
            "Total $100,00\r\n" +
            "\r\n" +
            "Savings Account\r\n" +
            "  deposit $4.000,00\r\n" +
            "  withdrawal $200,00\r\n" +
            "Total $3.800,00\r\n" +
            "\r\n" +
            "Total In All Accounts $3.900,00";

            Assert.AreEqual(statement, CustomerOperations.GetStatement(henry));
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar");            
            CustomerOperations.OpenAccount(new SavingsAccount((int)AccountType.SAVINGS), oscar);
            Assert.AreEqual(1, CustomerOperations.GetNumberOfAccounts(oscar));
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar");
            CustomerOperations.OpenAccount(new SavingsAccount((int)AccountType.SAVINGS), oscar);
            CustomerOperations.OpenAccount(new CheckingAccount((int)AccountType.CHECKING), oscar);
            Assert.AreEqual(2, CustomerOperations.GetNumberOfAccounts(oscar));
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar");
            CustomerOperations.OpenAccount(new SavingsAccount((int)AccountType.SAVINGS), oscar);
            CustomerOperations.OpenAccount(new CheckingAccount((int)AccountType.CHECKING), oscar);
            CustomerOperations.OpenAccount(new CheckingAccount((int)AccountType.MAXI_SAVINGS), oscar);
            Assert.AreEqual(3, CustomerOperations.GetNumberOfAccounts(oscar));
        }
    }
}
