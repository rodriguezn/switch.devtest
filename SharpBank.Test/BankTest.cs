﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;
        
        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            BankOperations.AddCustomer(john, bank);
            CustomerOperations.OpenAccount(new CheckingAccount((int)AccountType.CHECKING), john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", BankOperations.CustomerSummary(bank));
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();            
            Customer bill = new Customer("Bill");
            BankOperations.AddCustomer(bill, bank);
            Account checkingAccount = new CheckingAccount((int)AccountType.CHECKING);
            CustomerOperations.OpenAccount(checkingAccount, bill);            

            checkingAccount.Deposit(100.0);

            Assert.AreEqual(0.1, BankOperations.TotalInterestPaid(bank), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            Customer bill = new Customer("Bill");
            BankOperations.AddCustomer(bill, bank);
            Account savingAccount = new SavingsAccount((int)AccountType.SAVINGS);            
            CustomerOperations.OpenAccount(savingAccount, bill);

            savingAccount.Deposit(1500.0);
            
            Assert.AreEqual(2.0, BankOperations.TotalInterestPaid(bank), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccount()
        {
            Bank bank = new Bank();
            Customer bill = new Customer("Bill");
            BankOperations.AddCustomer(bill, bank);
            Account maxiAccount = new MaxiSavings((int)AccountType.MAXI_SAVINGS);
            CustomerOperations.OpenAccount(maxiAccount, bill);

            maxiAccount.Deposit(3000.0);            

            Assert.AreEqual(150.0, BankOperations.TotalInterestPaid(bank), DOUBLE_DELTA);
        } 
    }
}
