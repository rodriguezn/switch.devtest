﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public static class CommonFunctions
    {
        //Make sure correct plural of word is created based on the number passed in:
        //If number passed in is 1 just return the word otherwise add an 's' at the end
        public static String Format(int number, String word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }

        public static String ToDollars(double amount)
        {
            return String.Format("${0:N2}", Math.Abs(amount));
        }
    }
}
