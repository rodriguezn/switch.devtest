﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public static class BankOperations
    {
        public static void AddCustomer(Customer customer, Bank bank)
        {
            bank.customers.Add(customer);
        }

        public static String CustomerSummary(Bank bank)
        {
            String summary = "Customer Summary";
            foreach (Customer customer in bank.customers)
                summary += "\n - " + customer.Name + " (" + CommonFunctions.Format(CustomerOperations.GetNumberOfAccounts(customer), "account") + ")";
            return summary;
        }

        public static double TotalInterestPaid(Bank bank)
        {
            double total = 0;
            foreach (Customer customer in bank.customers)
                total += CustomerOperations.TotalInterestEarned(customer);
            return total;
        }

        public static String GetFirstCustomer(Bank bank)
        {
            try
            {
                string firstClientName = bank.customers.Count > 0 ? bank.customers[0].Name : "This bank does not have any clients.";
                return firstClientName;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "Error";
            }
        }
    }
}
