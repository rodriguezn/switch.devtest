﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Customer
    {
        public String Name { get; set; }
        public List<Account> Accounts { get; set; }       

        public Customer(String name)
        {
            this.Name = name;
            this.Accounts = new List<Account>();
        }     
    }
}
