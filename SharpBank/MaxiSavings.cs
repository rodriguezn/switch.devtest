﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class MaxiSavings : Account
    {
        public MaxiSavings(int accountType) : base(accountType)
        {
        }

        public override double InterestEarned()
        {
            double amount = SumTransactions();
            bool invalidWithdraw = CheckForWithdraw();

            if (invalidWithdraw)
                return amount * 0.001;
            return amount * 0.05;
        }

        public bool CheckForWithdraw()
        {
            int topDays = 10;
            foreach (Transaction transaction in transactions)
            {
                if (transaction.amount < 0)
                {
                    TimeSpan timeSpanDifference = DateTime.Now.Subtract(transaction.transactionDate);

                    if (timeSpanDifference.Days <= topDays)
                        return true;
                }
            }
            return false;
        } 
    }
}
