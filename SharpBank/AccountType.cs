﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public enum AccountType
    {
        CHECKING,
        SAVINGS,
        MAXI_SAVINGS
    }
}
