﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public static class CustomerOperations
    { 
        public static Customer OpenAccount(Account account, Customer customer)
        {
            customer.Accounts.Add(account);
            return customer;
        }

        public static int GetNumberOfAccounts(Customer customer)
        {
            return customer.Accounts.Count;
        }

        public static double TotalInterestEarned(Customer customer)
        {
            double total = 0;
            foreach (Account account in customer.Accounts)
                total += account.InterestEarned();
            return total;
        }

        public static double TransferToAccount(Account originAccount, Account destinyAccount, double amount)
        {
            //verifying the origin account
            if (originAccount.SumTransactions() >= amount)
            {
                originAccount.Withdraw(amount);
                destinyAccount.Deposit(amount);
            }
            return originAccount.SumTransactions();
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public static String GetStatement(Customer customer)
        {
            StringBuilder strBuilder = new StringBuilder("Statement for ");
            strBuilder.Append(customer.Name);
            strBuilder.Append(Environment.NewLine);
            strBuilder.Append(Environment.NewLine);            
            double total = 0.0;
            foreach (Account account in customer.Accounts)
            {                
                strBuilder.AppendLine(StatementForAccount(account));
                total += account.SumTransactions();
            }            
            strBuilder.Append("Total In All Accounts " + CommonFunctions.ToDollars(total));
            return strBuilder.ToString();            
        }

        public static String StatementForAccount(Account account)
        {
            StringBuilder strBuilder = new StringBuilder();
            String accountStatement = String.Empty;

            //Translate to pretty account type
            switch (account.GetAccountType())
            {
                case (int)AccountType.CHECKING:                    
                    strBuilder.AppendLine("Checking Account");
                    break;
                case (int)AccountType.SAVINGS:                    
                    strBuilder.AppendLine("Savings Account");
                    break;
                case (int)AccountType.MAXI_SAVINGS:                    
                    strBuilder.AppendLine("Maxi Savings Account");
                    break;
            }

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction transaction in account.transactions)
            {                
                string strTypeTransaction = transaction.amount < 0 ? "  withdrawal " : "  deposit ";
                strBuilder.Append(strTypeTransaction);
                strBuilder.AppendLine(CommonFunctions.ToDollars(transaction.amount));
                total += transaction.amount;
            }            
            strBuilder.AppendLine("Total " + CommonFunctions.ToDollars(total));            
            return strBuilder.ToString();
        }               
    }
}
