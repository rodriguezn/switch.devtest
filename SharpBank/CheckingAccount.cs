﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class CheckingAccount : Account
    {                
        public CheckingAccount(int accountType) : base(accountType)
        {
        }
              
        public override double InterestEarned()
        {
            return base.InterestEarned();
        }        
    }
}
