﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Account
    {
        public readonly int accountType;
        public List<Transaction> transactions;

        public Account(int accountType)
        {
            this.accountType = accountType;
            this.transactions = new List<Transaction>();
        }

        public virtual void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount));
            }
        }

        public virtual void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(-amount));
            }
        }

        public virtual double SumTransactions()
        {
            double amount = 0.0;
            foreach (Transaction transaction in transactions)
                amount += transaction.amount;
            return amount;
        }

        public virtual double InterestEarned()
        {
            double amount = SumTransactions();
            return amount * 0.001;
        }

        public virtual int GetAccountType()
        {
            return accountType;
        }

    }
}